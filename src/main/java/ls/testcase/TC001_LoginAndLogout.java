package ls.testcase;

import java.util.concurrent.TimeUnit;

import org.openqa.selenium.WebElement;
import org.testng.annotations.Test;

import ls.design.ProjectMethods;

public class TC001_LoginAndLogout extends ProjectMethods{
    @Test
	public void Login1() {
	WebElement eleimage = locateElement("xpath","//a[@class='gb_b gb_hb gb_R']");
	click(eleimage);
	driver.manage().timeouts().implicitlyWait(3000, TimeUnit.SECONDS);
	WebElement elesignout = locateElement("xpath","//a[contains(text(),'Sign out')]");
	click(elesignout);
	
	}
	
}
