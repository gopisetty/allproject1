package ls.api;

import java.io.File;
import java.io.IOException;
import java.util.ArrayList;
import java.util.List;
import java.util.Set;
import java.util.concurrent.TimeUnit;

import org.apache.commons.io.FileUtils;
import org.openqa.selenium.Alert;
import org.openqa.selenium.OutputType;
import org.openqa.selenium.WebElement;
import org.openqa.selenium.chrome.ChromeDriver;
import org.openqa.selenium.firefox.FirefoxDriver;
import org.openqa.selenium.remote.RemoteWebDriver;
import org.openqa.selenium.support.ui.Select;

import ls.base.Browser;
import ls.base.Element;

public class SeleniumBase implements Browser, Element{

	public RemoteWebDriver driver;
	public int i =1;
	@Override
	public void startApp(String url) {
		// TODO Auto-generated method stub

	}

	@Override
	public void startApp(String browser, String url) {
		if(browser.equalsIgnoreCase("chrome")) {
			System.setProperty("webdriver.chrome.driver", "./driver/chromedriver.exe");
			driver = new ChromeDriver();
		} else if(browser.equalsIgnoreCase("firefox")) {
			System.setProperty("webdriver.gecko.driver", "./driver/geckodriver.exe");
			driver = new FirefoxDriver(); 
		}
		driver.manage().window().maximize();
		driver.get(url);
		driver.manage().timeouts().implicitlyWait(30, TimeUnit.SECONDS);
		System.out.println("The browser "+browser+" launched successfully");
        takeSnap();
	}

	@Override
	public WebElement locateElement(String locatorType, String value) {
		switch (locatorType) {
		case "id": return driver.findElementById(value);
		case "name": return driver.findElementByName(value);
		case "class": return driver.findElementByClassName(value);
		case "xpath": return driver.findElementByXPath(value);
		case "partiallinktext": return driver.findElementByPartialLinkText(value);
		case "linktext" : return driver.findElementByLinkText(value);
		default:
			break;
		}
		return null;
	}

	@Override
	public WebElement locateElement(String value) {
		// TODO Auto-generated method stub
		return null;
	}

	@Override
	public List<WebElement> locateElements(String type, String value) {
		
		// TODO Auto-generated method stub
		return null;
	}

	@Override
	public void switchToAlert() {
		// TODO Auto-generated method stub

		Alert alert = driver.switchTo().alert();
		
		
	}

	@Override
	public void acceptAlert() {
		// TODO Auto-generated method stub
		Alert alert = driver.switchTo().alert();
		alert.accept();
	}

	@Override
	public void dismissAlert() {
		// TODO Auto-generated method stub
		Alert alert = driver.switchTo().alert();
        alert.dismiss();
	}

	@Override
	public String getAlertText() {
		// TODO Auto-generated method stub
		Alert alert = driver.switchTo().alert();
		String text = alert.getText();
		return text;
	}

	@Override
	public void typeAlert(String data) {
		// TODO Auto-generated method stub
		Alert alert = driver.switchTo().alert();
        alert.sendKeys(data);
        System.out.println("Enter the Text in Alert"+data+"is successfully passed");
		
	}

	@Override
	public void switchToWindow(int index) {
		
		Set<String> select = driver.getWindowHandles();
		List <String> ls = new ArrayList<>();
		ls.addAll(select);
		driver.switchTo().window(ls.get(index));	

	}

	@Override
	public void switchToWindow(String title) {
		// TODO Auto-generated method stub
		driver.switchTo().window(title);
		
	}

	@Override
	public void switchToFrame(int index) {
		// TODO Auto-generated method stub

	}

	@Override
	public void switchToFrame(WebElement ele) {
		// TODO Auto-generated method stub

	}

	@Override
	public void switchToFrame(String idOrName) {
		// TODO Auto-generated method stub

	}

	@Override
	public void defaultContent() {
		// TODO Auto-generated method stub

	}

	@Override
	public boolean verifyUrl(String url) {
		// TODO Auto-generated method stub
		return false;
	}

	@Override
	public boolean verifyTitle(String title) {
		// TODO Auto-generated method stub
		
		return false;
	}

	@Override
	public void takeSnap() {
		File src = driver.getScreenshotAs(OutputType.FILE);
		File des = new File("./snaps/img"+i+".png");
		try {
			FileUtils.copyFile(src, des);
		} catch (IOException e) {
			e.printStackTrace();
		}
		i++;
	}

	@Override
	public void close() {
		// TODO Auto-generated method stub

		driver.close();
	}

	@Override
	public void quit() {
		// TODO Auto-generated method stub
		driver.quit();
	}

	@Override
	public void click(WebElement ele) {
		ele.click();
		System.out.println("The element "+ele+" clicked successfully");
		takeSnap();
		
	}

	@Override
	public void append(WebElement ele, String data) {
		// TODO Auto-generated method stub
		
	}

	@Override
	public void clear(WebElement ele) {
		// TODO Auto-generated method stub
		ele.clear();
	}

	@Override
	public void clearAndType(WebElement ele, String data) {
		ele.clear();
		ele.sendKeys(data); 
		System.out.println("The data "+data+" entered successfully");
		takeSnap();
	}

	@Override
	public String getElementText(WebElement ele) {
		// TODO Auto-generated method stub
		return null;
	}

	@Override
	public String getBackgroundColor(WebElement ele) {
		// TODO Auto-generated method stub
		return null;
	}

	@Override
	public String getTypedText(WebElement ele) {
		// TODO Auto-generated method stub
		return null;
	}

	@Override
	public void selectDropDownUsingText(WebElement ele, String value) {
		// TODO Auto-generated method stub
		Select sel = new Select(ele);
		
		sel.selectByVisibleText(value);
		
		
	}

	@Override
	public void selectDropDownUsingIndex(WebElement ele, int index) {
		// TODO Auto-generated method stub
	
		Select sel = new Select(ele);
		sel.selectByIndex(index);
		
	}

	@Override
	public void selectDropDownUsingValue(WebElement ele, String value) {
		// TODO Auto-generated method stub
		Select sel = new Select(ele);
		sel.selectByValue(value);
	}

	@Override
	public boolean verifyExactText(WebElement ele, String expectedText) {
		// TODO Auto-generated method stub
		//boolean exacttext = ele.
		return false;
	}

	@Override
	public boolean verifyPartialText(WebElement ele, String expectedText) {
		// TODO Auto-generated method stub
		boolean partialtext = ele.getText().equals(expectedText);
		return partialtext;
	}

	@Override
	public boolean verifyExactAttribute(WebElement ele, String attribute, String value) {
		// TODO Auto-generated method stub
		boolean exactattribute = ele.getAttribute(attribute).equals(value);
		
		return exactattribute;
	}

	@Override
	public void verifyPartialAttribute(WebElement ele, String attribute, String value) {
		// TODO Auto-generated method stub
		ele.getAttribute(attribute).contains(value);
	}

	@Override
	public boolean verifyDisplayed(WebElement ele) {
		// TODO Auto-generated method stub
		boolean displayed = ele.isDisplayed();
		
		return displayed;
	}

	@Override
	public boolean verifyDisappeared(WebElement ele) {
		// TODO Auto-generated method stub
		
		return false;
	}

	@Override
	public boolean verifyEnabled(WebElement ele) {
		// TODO Auto-generated method stub
		boolean enabled = ele.isEnabled();
		return enabled;
	}

	@Override
	public boolean verifySelected(WebElement ele) {
		// TODO Auto-generated method stub
		boolean selected = ele.isSelected();
		return selected;
	}

}
