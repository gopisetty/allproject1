package week3day2;

import org.openqa.selenium.WebElement;
import org.openqa.selenium.chrome.ChromeDriver;

public class Task7 {

	public static void main(String[] args) throws InterruptedException {
		// TODO Auto-generated method stub

		System.setProperty("webdriver.chrome.driver","./driver/chromedriver.exe");
		ChromeDriver driver = new   ChromeDriver();
		driver.manage().window().maximize();
		driver.get("http://leaftaps.com/opentaps/control/main");
		
		driver.findElementById("username").sendKeys("DemoSalesManager");
		driver.findElementById("password").sendKeys("crmsfa");
		driver.findElementByClassName("decorativeSubmit").click();
		
		driver.findElementByLinkText("CRM/SFA").click();
		driver.findElementByLinkText("Create Lead").click();
		
		driver.findElementByXPath("//a[text()='Find Leads']").click();
		
		Thread.sleep(2000);
		
		driver.findElementById("ext-gen248").sendKeys("Gopi");
		
		driver.findElementByClassName("x-btn-center").click();
		
		driver.findElementByLinkText("10143").click();
		
		Thread.sleep(2000);
		
		System.out.println(driver.getTitle());
		
		driver.findElementByXPath("//a[text()='Edit']").click();
		WebElement ele = driver.findElementByXPath("//input[@id='updateLeadForm_companyName']");

		ele.clear();
		ele.sendKeys("Wipro");
		WebElement ele1 = driver.findElementByXPath("//input[@value='Update']");
		ele1.click();
		WebElement name = driver.findElementById("viewLead_companyName_sp");
		String str = name.getText();System.out.println(str);
		if(str.contains("Wipro"))
		{
			System.out.println("Correct");
		}
		else
		{
			System.out.println("Wrong");
		}
		driver.close();
	}

}
