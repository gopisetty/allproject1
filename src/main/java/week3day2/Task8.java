package week3day2;

import java.util.ArrayList;
import java.util.List;
import java.util.Set;

import org.openqa.selenium.Alert;
import org.openqa.selenium.chrome.ChromeDriver;

public class Task8 {

	public static void main(String[] args) throws InterruptedException {
		// TODO Auto-generated method stub

		System.setProperty("webdriver.chrome.driver","./driver/chromedriver.exe");
		ChromeDriver driver = new   ChromeDriver();
		driver.manage().window().maximize();
		driver.get("http://leaftaps.com/opentaps/control/main");
		
		driver.findElementById("username").sendKeys("DemoSalesManager");
		driver.findElementById("password").sendKeys("crmsfa");
		driver.findElementByClassName("decorativeSubmit").click();
		
		driver.findElementByLinkText("CRM/SFA").click();
		driver.findElementByLinkText("Create Lead").click();
		driver.findElementByXPath("//a[text()='Merge Leads']").click();
		driver.findElementByXPath("//img[@src='/images/fieldlookup.gif']").click();
		
		Set<String> select = driver.getWindowHandles();
		List <String> ls = new ArrayList<>();
		ls.addAll(select);
		driver.switchTo().window(ls.get(1));
		
		driver.findElementByXPath("//*[@id=\"ext-gen25\"]").sendKeys("10108");
		driver.findElementByClassName("x-btn-text").click();
		Thread.sleep(3000);
		driver.findElementByLinkText("10108").click();
		
		driver.switchTo().window(ls.get(0));
		driver.findElementByXPath("//input[@id='partyIdTo']/following-sibling::a").click();
		
		//Thread.sleep(2000);
		driver.switchTo().window(ls.get(2));
		driver.findElementByXPath("//*[@id=\"ext-gen25\"]").sendKeys("10018");
		driver.findElementByClassName("x-btn-text").click();
		Thread.sleep(3000);
		driver.findElementByLinkText("10108").click();
		driver.findElementByXPath("//a[@class='buttonDangerous']").click();
		
		Alert alert = driver.switchTo().alert();
		alert.accept();
		
	}

}
