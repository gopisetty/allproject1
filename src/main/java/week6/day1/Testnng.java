package week6.day1;

import org.testng.annotations.Test;

public class Testnng {

	//Priority will decide the order 
	@Test(priority=1)
	public void openBrowser()
	{
		System.out.println("I am openBrowser");
	}
	@Test(priority=2)
	public void goToURL()
	{
		System.out.println("I am goToURL");
	}
    @Test(priority=3)
	public void clickGmail()
	{
		System.out.println("I am clickGmail");
	}
	
	
}
