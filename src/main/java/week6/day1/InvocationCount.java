package week6.day1;

import org.testng.annotations.Test;

public class InvocationCount {

	//InvocationCount acts as a "forloop" it will execute two times
	//InvocationCount is a attribute
	
	//THREADCOUNT :-
	//The threadcount attribute is normally set via the TestNG suite xml file. It basically determines the intensity of parallelism for your testng tests [ ie., how many @Test annotated test methods can TestNG run in parallel at any given point in time ]
	/*The default number of threads to use when running tests in parallel.	
	This sets the default maximum number of threads to use for running tests in parallel. 
	It will only take effect if the parallel mode has been thread count selected 
	(for example, with the -parallel option). This can be overridden in the suite definition.*/
 
	
	//THREADPOOLSIZE :-
	//The threadPoolSize is an attribute of the @Test annotation. This attribute informs TestNG how many threads should testng spawn for the current @Test annotated test method. This attribute is relevant only when you use the invocationCount attribute of the @Test annotation.
	//threadPoolSize	The size of the thread pool for this method. The method will be invoked from multiple threads as specified by invocationCount. 
	//Note: this attribute is ignored if invocationCount is not specified.
	
	
	@Test(invocationCount = 2)
	public void openBrowser()
	{
		System.out.println("I am openBrowser");
	}
	@Test
	public void goToURL()
	{
		System.out.println("I am goToURL");
	}
    @Test
	public void clickGmail()
	{
		System.out.println("I am clickGmail");
	}

	
	
	
	
	
}
