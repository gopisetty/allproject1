package week6.day1;

import org.testng.annotations.Test;

public class Ignore {

	//Ignore (enabled) will not execute the specific method 
	
	//syntax : @Test(enabled = false)
		@Test(enabled = false)
		public void openBrowser()
		{
			System.out.println("I am openBrowser");
		}
		@Test
		public void goToURL()
		{
			System.out.println("I am goToURL");
		}
	    @Test
		public void clickGmail()
		{
			System.out.println("I am clickGmail");
		}
	
	
}
