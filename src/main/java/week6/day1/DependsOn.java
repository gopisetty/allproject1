package week6.day1;

import org.testng.annotations.Test;

public class DependsOn {
	
	//Depends on method will execute last once the openBrowser executed then only depended method will execute
	
	@Test
	public void openBrowser()
	{
		System.out.println("I am openBrowser");
	}
	@Test(dependsOnMethods="openBrowser")
	public void goToURL()
	{
		System.out.println("I am goToURL");
	}
    @Test(dependsOnMethods="goToURL")
	public void clickGmail()
	{
		System.out.println("I am clickGmail");
	}

	
	
	

}
