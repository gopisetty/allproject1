package week6.day1;

import org.openqa.selenium.WebElement;
import org.openqa.selenium.chrome.ChromeDriver;
import org.testng.annotations.DataProvider;
import org.testng.annotations.Test;

public class Dataproviders {
ChromeDriver driver;
	@DataProvider(name = "Authentication")
	public static Object[][] data()
	{
		return new Object[][] {{"yasaswini.gopisetty@gmail.com","Q$61e5514"},{"gopisetty.yasaswini@gmail.com","Q$61e5516"}};
	}
	@Test(dataProvider="Authentication")
	public void test(String Username,String Password) throws InterruptedException
	{
		System.setProperty("webdriver.chrome.driver", "./driver/chromedriver.exe");
		driver = new ChromeDriver();
		driver.get("https://accounts.google.com/ServiceLogin/identifier?service=mail&flowName=GlifWebSignIn&flowEntry=ServiceLogin");
		driver.manage().window().maximize();
		WebElement eleUsername = driver.findElementByXPath("//input[@id='identifierId']");//identifierId
		eleUsername.sendKeys(Username);
		
		WebElement eleNext = driver.findElementByXPath("//span[contains(text(),'Next')]");
		eleNext.click();
		Thread.sleep(3000);
		
		WebElement elePassword = driver.findElementByXPath("//input[@name='password']");//password
		elePassword.sendKeys(Password); 
			
		WebElement eleNext2 = driver.findElementByXPath("//span[contains(text(),'Next')]");
		eleNext2.click();
	}
}
