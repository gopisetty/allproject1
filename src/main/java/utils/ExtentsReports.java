package utils;

import java.io.IOException;

import com.aventstack.extentreports.ExtentReports;
import com.aventstack.extentreports.ExtentTest;
import com.aventstack.extentreports.MediaEntityBuilder;
import com.aventstack.extentreports.reporter.ExtentHtmlReporter;

import ls.api.SeleniumBase;

public class ExtentsReports extends SeleniumBase{
	
	ExtentHtmlReporter html;
	ExtentReports extent;
	ExtentTest test;
	public void startReport() {

	 html = new ExtentHtmlReporter("./report/extentReport.html");
	
	 extent = new ExtentReports();
	 
	 html.setAppendExisting(true);
	 
	 extent.attachReporter(html);
	
	
	}
	
	public void report(String testcaseName,String testDes,String as) throws IOException
	{
	   test = extent.createTest("","");
		test.assignAuthor("yasaswini");
		test.assignCategory("smoke");
		test.pass("",MediaEntityBuilder.createScreenCaptureFromPath("./../snaps/img1.png").build());
		extent.flush();
		
		
		
	}
	
}
