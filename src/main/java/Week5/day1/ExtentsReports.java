package Week5.day1;

import java.io.IOException;

import org.testng.annotations.Test;

import com.aventstack.extentreports.ExtentReports;
import com.aventstack.extentreports.ExtentTest;
import com.aventstack.extentreports.MediaEntityBuilder;
import com.aventstack.extentreports.reporter.ExtentHtmlReporter;

import week3.day1.SeleniumBasic;

public class ExtentsReports {
	
	ExtentHtmlReporter html;//
	ExtentReports extent;
	ExtentTest test;
	
	public void startReport() {
		html = new ExtentHtmlReporter("./report/extentReport.html");
		
		//logs 
		extent = new ExtentReports();
		
		//if you are running 100 test cases and wanted to have everything in collade one
		html.setAppendExisting(true);
		
		//log reports attached to template
		extent.attachReporter(html);
	}
	
	
	public void runReport() throws IOException
	{
		//Passing HTML path where your path need to store
		
		
		//extent will hold 100 logs if we want to call a particular test case then call it by testcase name and description
		test = extent.createTest("TC001_Login", "Login into LeafTaps");
		
		//Author name who created it
		test.assignAuthor("Yasaswini");
		
		//which type of testing you wanted to perform
		test.assignCategory("smoke");
		
		
		test.pass("Username DemosalesManager entered successfully", 
				MediaEntityBuilder.createScreenCaptureFromPath("./../snaps/img1.png").build());
		
		extent.flush();
		
	}
	
	

}
