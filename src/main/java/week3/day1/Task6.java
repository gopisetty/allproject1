package week3.day1;

import java.util.List;

import org.openqa.selenium.WebElement;
import org.openqa.selenium.chrome.ChromeDriver;
import org.openqa.selenium.support.ui.Select;

public class Task6 {

	public static void main(String[] args) {
		// TODO Auto-generated method stub
		System.setProperty("webdriver.chrome.driver", "./driver/chromedriver.exe");
		ChromeDriver driver = new ChromeDriver();
		driver.manage().window().maximize();
		driver.get("http://www.leafground.com/pages/Dropdown.html");
        WebElement ele = driver.findElementById("dropdown1");

        Select sel = new Select(ele);
        sel.selectByIndex(1);
        WebElement ele1 = driver.findElementByName("dropdown2");

        Select sel1 = new Select(ele1);

        sel1.selectByVisibleText("Appium");
        WebElement ele2 = driver.findElementById("dropdown3");
        Select sel2 = new Select(ele2);
        sel2.selectByValue("3");
        
        WebElement ele3 = driver.findElementByClassName("dropdown");
        Select sel3 = new Select(ele3);
        
        List <WebElement> el = sel3.getOptions();
        int x = el.size();
        System.out.println(x);
        
        WebElement ele4 = driver.findElementByXPath("//option[text()='You can also use sendKeys to select']/..");
        ele4.sendKeys("Loadrunner");
        
        WebElement ele5 = driver.findElementByXPath("//option[@disabled='true']");
        Select sel31 = new Select(ele5);
        sel31.isMultiple();
        System.out.println(sel31);
       
	}

}
