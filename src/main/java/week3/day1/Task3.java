package week3.day1;

import org.openqa.selenium.Dimension;
import org.openqa.selenium.Point;
import org.openqa.selenium.WebElement;
import org.openqa.selenium.chrome.ChromeDriver;

public class Task3 {

	public static void main(String[] args) {
		// TODO Auto-generated method stub
		System.setProperty("webdriver.chrome.driver", "./driver/chromedriver.exe");
		ChromeDriver driver = new ChromeDriver();
		driver.manage().window().maximize();
		driver.get("http://www.leafground.com/pages/Button.html");
		
		//driver.findElementById("home").click();
		WebElement ele = driver.findElementById("position");
		Point p = ele.getLocation();
		int x = p.getX();
		int y = p.getY();
		System.out.println(x);
		System.out.println(y);
		
		WebElement ele1 = driver.findElementById("color");
		String s1 = ele1.getCssValue("color");
		System.out.println(s1);
		
		WebElement ele2 = driver.findElementById("size");
		System.out.println(ele2.getSize());
		
		
		
		
	}

}
