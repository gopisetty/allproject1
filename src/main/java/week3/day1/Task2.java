package week3.day1;

import org.openqa.selenium.Keys;
import org.openqa.selenium.WebElement;
import org.openqa.selenium.chrome.ChromeDriver;

public class Task2 {

	public static void main(String[] args) {
		// TODO Auto-generated method stub

		
		System.setProperty("webdriver.chrome.driver", "./driver/chromedriver.exe");
		ChromeDriver driver = new ChromeDriver();
		driver.manage().window().maximize();
		driver.get("http://www.leafground.com/pages/Edit.html");
		driver.findElementById("email").sendKeys("gopisetty.yasaswini@gmail.com");
		
		WebElement ele = driver.findElementByXPath("//input[@value='Append ']");
		ele.clear();
		ele.sendKeys("Yasaswini",Keys.TAB);
		
		//
		WebElement ele1 = driver.findElementByXPath("//input[@name='username']");
		ele1.getAttribute("value");
		//System.out.println(ele1.getAttribute("value"));
		
		driver.findElementByXPath("//input[@value='Clear me!!']").clear();
		
		WebElement ele2 = driver.findElementByXPath("//input[@disabled='true']");
		if(ele2.isEnabled())
		{
			System.out.println("Enter the text");
		}
		else
		{
			System.out.println("Is disabled");
		}
		
	}

}
