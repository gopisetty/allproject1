package week3.day1;

import java.util.List;

import org.openqa.selenium.WebElement;
import org.openqa.selenium.chrome.ChromeDriver;
import org.openqa.selenium.support.ui.Select;

public class SeleniumBasic {

	public static void main(String[] args) {
		System.setProperty("webdriver.chrome.driver","./driver/chromedriver.exe");
		ChromeDriver driver = new   ChromeDriver();
		driver.manage().window().maximize();
		driver.get("http://leaftaps.com/opentaps/control/main");
		
		driver.findElementById("username").sendKeys("DemoSalesManager");
		driver.findElementById("password").sendKeys("crmsfa");
		driver.findElementByClassName("decorativeSubmit").click();
		
		driver.findElementByLinkText("CRM/SFA").click();
		driver.findElementByLinkText("Create Lead").click();
		
		driver.findElementById("createLeadForm_companyName").sendKeys("Wipro");
		driver.findElementById("createLeadForm_firstName").sendKeys("Yasaswini");
		driver.findElementById("createLeadForm_lastName").sendKeys("Gopisetty");
		
		WebElement ele = driver.findElementById("createLeadForm_dataSourceId");
		Select sel = new Select(ele);
		
		sel.selectByVisibleText("Cold Call");
		
		WebElement ele1 = driver.findElementById("createLeadForm_marketingCampaignId");
		Select sel1 = new Select(ele1);
		
		sel1.selectByValue("CATRQ_CARNDRIVER");
		
		//createLeadForm_industryEnumId
		WebElement ele2 = driver.findElementById("createLeadForm_industryEnumId");
		Select sel2 = new Select(ele2);
		String txt;
		List<WebElement> alloptions = sel2.getOptions();
		for(WebElement each :alloptions)
		{
			String str = each.getText();
			if(str.startsWith("M"))
			{
				txt=str;
				System.out.println(txt.toString());
			
		
		}
	

		}}}
