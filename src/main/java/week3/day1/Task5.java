package week3.day1;

import org.openqa.selenium.WebElement;
import org.openqa.selenium.chrome.ChromeDriver;

public class Task5 {

	public static void main(String[] args) {
		// TODO Auto-generated method stub
		System.setProperty("webdriver.chrome.driver", "./driver/chromedriver.exe");
		ChromeDriver driver = new ChromeDriver();
		driver.manage().window().maximize();
		driver.get("http://www.leafground.com/pages/checkbox.html");
		
		WebElement ele = driver.findElementByXPath("//input[@type='checkbox']");
		ele.click();
		
		//label[text()='Confirm Selenium is checked']/following-sibling::input
		WebElement ele1 = driver.findElementByXPath("//label[text()='Confirm Selenium is checked']/following-sibling::input");
		
		boolean checkbox = ele1.isSelected();
		System.out.println(checkbox);
		if(ele1.isSelected())
		{
			System.out.println("checkbox is selected");
		}
		else
		{
			System.out.println("checkbox is not selected");
		}
		
		WebElement ele2 = driver.findElementByXPath("//label[text()='DeSelect only checked']/following-sibling::input[1]");
		WebElement ele3 = driver.findElementByXPath("//label[text()='DeSelect only checked']/following-sibling::input[2]");
		if(ele2.isSelected())
		{
			ele2.click();
		}
		else
		{
			ele3.click();

		}
		

		WebElement ele4 = driver.findElementByXPath("//label[text()='Select all below checkboxes ']/following-sibling::input[1]");
		ele4.click();
		
		WebElement ele5 = driver.findElementByXPath("//label[text()='Select all below checkboxes ']/following-sibling::input[2]");
		ele5.click();

		WebElement ele6 = driver.findElementByXPath("//label[text()='Select all below checkboxes ']/following-sibling::input[3]");
		ele6.click();

		WebElement ele7 = driver.findElementByXPath("//label[text()='Select all below checkboxes ']/following-sibling::input[4]");
        ele7.click();
		WebElement ele8 = driver.findElementByXPath("//label[text()='Select all below checkboxes ']/following-sibling::input[5]");
        ele8.click();
		WebElement ele9 = driver.findElementByXPath("//label[text()='Select all below checkboxes ']/following-sibling::input[6]");
		ele9.click();
		
		}

		
		
		
		
	}


