package week4.day2;

import java.io.File;
import java.io.IOException;
import java.util.ArrayList;
import java.util.List;
import java.util.Set;
import java.util.concurrent.TimeUnit;

import org.apache.commons.io.FileUtils;
import org.openqa.selenium.Alert;
import org.openqa.selenium.By;
import org.openqa.selenium.OutputType;
import org.openqa.selenium.TakesScreenshot;
import org.openqa.selenium.WebElement;
import org.openqa.selenium.chrome.ChromeDriver;
import org.openqa.selenium.support.ui.ExpectedConditions;
import org.openqa.selenium.support.ui.Select;
import org.openqa.selenium.support.ui.WebDriverWait;

public class CommonMethods {

	
	public void allMethods() throws IOException, InterruptedException
	{
		//LAUNCH BROWSER
		System.setProperty("webdriver.chrome.driver", "./driver/chromedriver.exe");
		ChromeDriver driver = new ChromeDriver();
		driver.get("http://www.leafground.com/pages/Dropdown.html");
		driver.manage().window().maximize();
		driver.manage().timeouts().implicitlyWait(20, TimeUnit.SECONDS);
		
		
		//DROPDOWN
		WebElement ele = driver.findElementById("dropdown1");
		Select sel = new Select(ele);
		sel.selectByVisibleText("Selenium");
		
		//Selectvalue
		
		WebElement ele1 = driver.findElementById("dropdown2");
		Select sel1 = new Select(ele1);
		sel.selectByIndex(3);
		//select index
		
		WebElement ele2 = driver.findElementById("dropdown3");
		Select sel2 = new Select(ele2);
		sel.selectByValue("3");
		
		WebElement ele3 = driver.findElementByClassName("dropdown");
		Select sel3 = new Select(ele3);
		List <WebElement> list = sel3.getOptions();
		int count = list.size();
		
		System.out.println(count);
		
		//Checkbox
		boolean selected = driver.findElementById("").isSelected();
		
		//Title
		String title = driver.getTitle();
		
		//Current URL
		String currentURL = driver.getCurrentUrl();
		
		
		
		//Switch to window
	
	Set<String> select = driver.getWindowHandles();
	List <String> ls = new ArrayList<>();
	ls.addAll(select);
	driver.switchTo().window(ls.get(0));
	
	//Snapshot
	File src = driver.getScreenshotAs(OutputType.FILE);
	
	File dec = new File("");
	FileUtils.copyFile(src, dec);
	
	
	Alert alert =driver.switchTo().alert();
	alert.accept();
	alert.dismiss();
	alert.sendKeys("Yasaswini");
	alert.getText();
	
	//Switch to frames
	WebElement ele5 = driver.findElementById("");
	driver.switchTo().frame(0);
	driver.switchTo().frame("nameOrId");
	driver.switchTo().frame(driver.findElementById(""));
	
	//default content returns to html dom (it always go to superframe)
	driver.switchTo().defaultContent();
	
	//nested frame (this always go back to one level that is near parent)
	driver.switchTo().parentFrame();
	
	//WebTable
	WebElement  table = driver.findElementByXPath("table1");
	List<WebElement> row = table.findElements(By.tagName("tr"));
	
	List<WebElement> colums = row.get(3).findElements(By.tagName("td"));
	System.out.println(colums.get(2).getText());
	
	//waits
	Thread.sleep(2000);
	
	//implicit
	driver.manage().timeouts().implicitlyWait(20, TimeUnit.SECONDS);
	
	//Webdriver wait
	WebElement element = driver.findElementById("");
	WebDriverWait wait = new WebDriverWait(driver,10);
	wait.until(ExpectedConditions.elementToBeClickable(element));
	
	//locators
	WebElement ele6 = driver.findElementByName("");
	driver.findElementById("").click();
	driver.findElementByName("").getText();
	driver.findElementByClassName("").getAttribute("");
	driver.findElementByLinkText("").sendKeys("");
	driver.findElementByPartialLinkText("").getCssValue("");
	driver.findElementByTagName("").clear();
	driver.findElementByXPath("");
		
		
	}
}
