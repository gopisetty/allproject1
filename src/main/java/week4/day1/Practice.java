package week4.day1;

import java.util.ArrayList;
import java.util.List;
import java.util.Set;

import org.openqa.selenium.chrome.ChromeDriver;

public class Practice {

	public static void main(String[] args) throws InterruptedException {
		// TODO Auto-generated method stub

System.setProperty("webdriver.chrome.driver","./driver/chromedriver.exe");
		
		ChromeDriver driver = new   ChromeDriver();
		driver.manage().window().maximize();
		
		driver.get("https://www.irctc.co.in/nget/train-search");
		driver.findElementByXPath("//span[text()='AGENT LOGIN']").click();
		Thread.sleep(3000);
		driver.findElementByXPath("//a[text()='Contact Us']").click();
		
		Set<String> ele = driver.getWindowHandles();
		List<String> ls = new ArrayList<String>();
		
		ls.addAll(ele);
		
		driver.switchTo().window(ls.get(1));
		
		System.out.println(driver.getTitle());
		
		driver.switchTo().window(ls.get(0));
		driver.close();
		
		
		
		
		
	}

}
