package week4.day1;

import org.openqa.selenium.Alert;
import org.openqa.selenium.WebElement;
import org.openqa.selenium.chrome.ChromeDriver;

public class Classroom11 {

	public static void main(String[] args) {
		// TODO Auto-generated method stub
		System.setProperty("webdriver.chrome.driver","./driver/chromedriver.exe");
		
		ChromeDriver driver = new   ChromeDriver();
		driver.manage().window().maximize();
		
		driver.get("https://www.w3schools.com/js/tryit.asp?filename=tryjs_prompt");
		WebElement frameelement = driver.findElementById("iframeResult");
		
		driver.switchTo().frame(frameelement);
		
		driver.findElementByXPath("//button[text()='Try it']").click();
		
		Alert alert = driver.switchTo().alert();
		alert.sendKeys("Yasaswini");
		
		alert.accept();
		
		String message = driver.findElementById("demo").getText();
		if(message.contains("Yasaswini"))
		{
			System.out.println("Message displayed is" + message);
		}
		driver.switchTo().defaultContent();
				
	}

}
